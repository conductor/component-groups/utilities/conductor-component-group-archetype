package @packageName@;

import org.conductor.component.annotations.ComponentOption;
import org.conductor.component.annotations.Property;
import org.conductor.component.annotations.ReadOnlyProperties;
import org.conductor.component.annotations.ReadOnlyProperty;
import org.conductor.component.types.Component;
import org.conductor.component.types.DataType;
import org.conductor.database.Database;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.Map;

@org.conductor.component.annotations.Component(type = "myComponentType")
@ComponentOption(name = "firstOption", dataType = "string", defaultValue = "")
@ComponentOption(name = "secondOption", dataType = "integer", defaultValue = "0")
@ReadOnlyProperty(name = "testProperty", dataType = DataType.INTEGER, defaultValue = "0")
public class ExampleComponent extends Component {

    private static final Logger log = LoggerFactory.getLogger(ExampleComponent.class);

    public ExampleComponent(Database database, Map<String, Object> options) {
        super(database, options);
    }

    @Property(initialValue = "false")
    public void setMyPropertyValue(Boolean value) {
        try {
            updatePropertyValue("myPropertyValue", value);
        } catch (Exception e) {
            log.error("Failed to update property value.", e);
        }

    }

}
